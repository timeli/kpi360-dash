# KPI-360 Demos

### Running the demos
Run `run_kpi.py` with

1. `kpi_spec.json` for compressor performance
2. `kpi_spec2.json` for pump performance
3. `kpi_spec_hr.json` for heat rate KPI

### Notes
* `run_kpi_dynamic.py` is called by the *contributors* element to spin up a linked app to analyse the selected contributing factor (currently only used in the heat rate KPI).
* `kpi_temp.json` is a temporary file created when a linked app is spun up from the heat rate KPI app.
* Linked apps are not automatically stopped when the base app is stopped - they need to be manually stopped.