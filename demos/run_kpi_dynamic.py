from app_def import KpiApp
import sys
import json


def create_kpi_spec(property):
    return {
        "appParameters": {
            "assetList": [property + " Analysis"],
            "appName": property + " Analysis",
            "dateMin": "2020-01-02",
            "dateMax": "2021-06-30"
        },
        "layout": [
            {"header":  12},
            {"separator": 12},
            {"assetSelector": 12},
            {"separator": 12},
            {"distribution": 6, "timeSeries": 6},
            {"separator": 12},
            {"correlation": 12},
            {"separator": 12},
        ],
        "blockParameters": {
            "timeSeries": {"maxVars": 4}
        }
    }


if __name__ == '__main__':
    print(sys.argv)
    with open('kpi_temp.json', 'w') as tempobj:
        json.dump(create_kpi_spec(sys.argv[1]), tempobj, indent=4)
    app_new = KpiApp('kpi_temp.json')
    app_new.deploy('0.0.0.0', int(sys.argv[2]))
