from dash import html


def header_block(app_title):
    return html.Header([
        html.Img(src='/assets/logo.png', height=50, style={'display': 'inline-block'}),
        html.H3(children=app_title, style={'display': 'inline-block', 'margin-left': 20}),
    ])


def separator_block():
    return html.Div([
        html.Div('', style={'height': '10px'}),
        html.Hr(),
        html.Div('', style={'height': '10px'}),
    ])