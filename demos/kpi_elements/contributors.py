from dash import dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
import webbrowser
import os
import time


def contrib_block():
    return dbc.Row([
        dbc.Col([
            html.H5(children='Sensitivity Analysis', style={'margin-top': 10}),
            dcc.RadioItems(['Sunburst', 'Treemap', 'Icicle'], 'Treemap',
                           id='contrib-plot-type', inputStyle={'margin-right': 5},
                           style={'font-size': 6}),
            dcc.Graph(id='contrib-graph', style={'height': '400px'}),
            html.Div(children='Dummy', hidden=True, id='contrib-dummy')
        ])
    ])


def contrib_callbacks(app, asset_info, linked_app_details, linked_app_trigger):

    @app.callback(
        Output('contrib-graph', 'figure'),
        Input('asset', 'value'),
        Input('contrib-plot-type', 'value')
    )
    def update_contrib_graph(asset_name, plot_type):
        sensitivity_data = asset_info[asset_name]['Sensitivity']
        parameter_name = asset_name[:-9]  # Strip away the ' Analysis' part from the name

        fig = go.Figure()

        # fig.add_trace(go.Bar(
        #     name='Control',
        #     x=contributors, y=values,
        #     error_y=dict(type='data', array=bounds)
        # ))

        contributors = []
        parents = []
        vals = []

        def parse_sensitivity_tree(d, parent):
            current_node = list(d.keys())[0]
            d_nested = list(d.values())[0]
            if 'Mean' in d_nested.keys():
                val = d_nested['Mean']
            else:
                val = sum(
                    [parse_sensitivity_tree({k: v}, current_node) for k, v in d_nested.items()]
                )
            contributors.append(current_node)
            parents.append(parent)
            vals.append(val)
            return val

        parse_sensitivity_tree({parameter_name: sensitivity_data}, '')

        # Normalise values
        # The root node is the last item in the list and is the sum of all leaf nodes
        values = [i*100/vals[-1] for i in vals]

        fig.add_trace(getattr(go, plot_type)(
            branchvalues='total',
            labels=contributors,
            parents=parents,
            values=values,
            texttemplate='%{label}: %{value:.2f} %',
            root_color="lightgrey"
            # textinfo='label+value'
        ))

        fig.update_layout(
            title='%s (%s) Contributors' % (parameter_name, asset_info[asset_name]['Channels'][parameter_name]['Units']),
            title_font_size=16,
            title_xanchor='center',
            title_x=0.5,
            margin=dict(t=30, b=0, l=0, r=0),
            xaxis_title='Contributors',
            yaxis_title='Contribution (%)'
            # legend=dict(yanchor="bottom", y=0.9, xanchor="left", x=0.01),
        )
        return fig

    @app.callback(
        Output('contrib-dummy', 'children'),
        Input('contrib-graph', 'clickData')
    )
    def contrib_click(click_data):
        factor = click_data['points'][0]['label']
        print(factor)

        if factor in linked_app_details:
            if not linked_app_details[factor]['running']:
                # Spin up linked app for the selected factor only if isn't already running
                linked_app_details[factor]['running'] = True
                os.system('%s "%s" %s &' % (
                    linked_app_trigger, factor, linked_app_details[factor]['port']
                ))
                time.sleep(3)  # Give the new app a couple of seconds to spin up

            # Switch to new tab in browser for newly-spun-up or already-running app
            webbrowser.open_new_tab('http://127.0.0.1:%s/' % linked_app_details[factor]['port'])

        return None
