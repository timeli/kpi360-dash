from dash import dcc, html, Input, Output, State, callback_context
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
import plotly.express as px
import matplotlib.dates as mdates
import pandas as pd

FILTER_OPTIONS = ['<', '<=', '>', '>=', '==', '!=']
DEF_MAX_CORR_VARS = 5

def corr_block(asset_list, asset_info, date_min, date_max, max_corr_vars=None, mark_freq=None):
    default_asset = asset_list[0]
    date_min = pd.to_datetime(date_min).tz_localize('UTC')
    date_max = pd.to_datetime(date_max).tz_localize('UTC')
    if not max_corr_vars:
        max_corr_vars = DEF_MAX_CORR_VARS
    if not mark_freq:
        mark_freq = ((date_max - date_min) / 4).round('15D')
    dates = pd.date_range(date_min, date_max, freq=mark_freq) + pd.to_timedelta('1D')

    # Conversion to int because https://github.com/plotly/dash-core-components/issues/159
    marks = {int(mdates.date2num(date)): str(date)[:10] for date in dates}

    channel_list = list(asset_info[default_asset]['Channels'].keys())
    return dbc.Row([
        dbc.Col([
            html.H5(children='Correlation Analysis', style={'margin-top': 10}),
            html.H6(children='Select parameters (max. %i)' % max_corr_vars, style={'margin-top': 10}),
            dcc.Dropdown(channel_list, channel_list[0:2], id='corr-variables', multi=True),
            html.H6(children='Select plot type', style={'margin-top': 10}),
            dcc.RadioItems(['Correlation coeffs.', 'Scatter plots'], 'Scatter plots',
                           id='corr-plot-type', labelStyle={'display': 'block'},
                           inputStyle={'margin-right': 5}),
            dcc.Checklist(['Apply filter'], [], id='corr-enable-filter',
                          inputStyle={'margin-right': 5, 'margin-top': 20}),
            html.Div([
                dbc.Row([
                    dbc.Col([
                        dcc.Dropdown(channel_list, channel_list[0], id='corr-filter-var',
                                     style={'display': 'inline-block', 'width': '200px'}),
                    ]),
                    dbc.Col([
                        dcc.Dropdown(FILTER_OPTIONS, '>', id='corr-filter-condition',
                                     style={'display': 'inline-block', 'width': '50px'}),
                    ]),
                    dbc.Col([
                        dcc.Input(id='corr-filter-value', type='number', style={'width': '100px'})
                    ],
                        style={'width': '100px'}
                    )
                ])
            ],
                id='corr-filter-block', hidden=True, style={'margin-top': 10, 'margin-bottom': 20}
            ),
            html.H6(children='Select date range', style={'margin-top': 10}),
            # Conversion to int because https://github.com/plotly/dash-core-components/issues/159
            dcc.RangeSlider(int(mdates.date2num(date_min)), int(mdates.date2num(date_max)),
                            value=[int(mdates.date2num(date_min)), int(mdates.date2num(date_max))],
                            marks=marks, id='corr-time-selector', updatemode='drag', step=1
                            ),
            html.Div('Selected date range: %s to %s' % (str(date_min)[:10], str(date_max)[:10]),
                     id='corr-date-range-output', style={'margin-top': 10}),
            html.Button('Run correlation analysis', id='corr-trigger', n_clicks=0,
                        style={'margin-top': 15, 'margin-bottom': 5,
                               'margin-left': 50, 'height': '60px', 'width': '150px'}),
        ],
            md=3, style={'margin-left': 20}
        ),
        dbc.Col([
            dcc.Graph(id='corr-graph', style={'width': '100%', 'height': '400px'}),
        ],
            md=8, style={'margin-left': 20}
        )
    ])


def corr_callbacks(app, data, asset_info, max_corr_vars=None):
    if not max_corr_vars:
        max_corr_vars = DEF_MAX_CORR_VARS

    @app.callback(
        Output('corr-filter-block', 'hidden'),
        Input('corr-enable-filter', 'value')
    )
    def make_corr_filter_visible(corr_enable_filter):
        return not len(corr_enable_filter)

    @app.callback(
        Output('corr-date-range-output', 'children'),
        Input('corr-time-selector', 'value'),
    )
    def update_corr_date_range_displayed(date_range):
        start_date = str(mdates.num2date(date_range[0]))[:10]
        end_date = str(mdates.num2date(date_range[1]))[:10]
        return 'Selected date range: %s to %s' % (start_date, end_date)

    @app.callback(
        Output('corr-graph', 'style'),
        Output('corr-graph', 'figure'),
        Input('corr-trigger', 'n_clicks'),
        State('corr-variables', 'value'),
        State('asset', 'value'),
        State('corr-plot-type', 'value'),
        State('corr-time-selector', 'value'),
        State('corr-enable-filter', 'value'),
        State('corr-filter-var', 'value'),
        State('corr-filter-condition', 'value'),
        State('corr-filter-value', 'value')
    )
    def plot_corr_analysis(n_clicks, variables, asset_name, plot_type, date_range,
                           enable_filter, filter_var, filter_cond, filter_val):
        """
        Plot correlation graph - correlation coefficients or scatter plots
        :param n_clicks:
        :param variables:
        :param asset_name:
        :param plot_type:
        :param date_range:
        :param enable_filter:
        :param filter_var:
        :param filter_cond:
        :param filter_val:
        :return:
        """
        if n_clicks > 0:  # to prevent the first callback run-through
            start_date = mdates.num2date(date_range[0])
            end_date = mdates.num2date(date_range[1])
            df = data[asset_name].loc[start_date:end_date]
            if enable_filter:
                df['Filter'] = [
                    'Filtered' if str(x) != 'nan' and eval(str(x) + filter_cond + str(filter_val))
                    else 'Unfiltered' for x in df[filter_var]
                ]
                colour_map = {'Filtered': 'green', 'Unfiltered': 'red'}
            # df = df
            if plot_type == 'Scatter plots':
                style = {'width': '100%', 'height': '%ipx' % (100 + 150 * len(variables))}
                if 'Filter' in df:
                    fig = px.scatter_matrix(df, dimensions=variables, color='Filter',
                                            color_discrete_map=colour_map)
                else:
                    fig = px.scatter_matrix(df, dimensions=variables, color=None)
                fig.update_traces(diagonal_visible=False)
                # fig = go.Figure(data=go.Splom(
                #     dimensions=[dict(label=x, values=df[x]) for x in df[variables].columns]
                # ))
            else:
                # fig = px.imshow(df[variables].corr(), zmin=-1, zmax=1, color_continuous_scale='RdBu_r', text_auto='.3f')
                style = {'width': '60%', 'height': '%ipx' % (100 + 100 * len(variables)),
                         'margin-left': '20%'}
                if enable_filter:
                    a = df[variables][df['Filter'] == 'Filtered'].corr()
                else:
                    a = df[variables].corr()
                a = a.reindex(index=a.index[::-1])
                fig = go.Figure(data=go.Heatmap(
                    z=a, x=df[variables].columns, y=list(reversed(variables)), zmin=-1, zmax=1,
                    colorscale='RdBu_r', text=a, texttemplate='%{text:.3f}'
                ))
            fig.update_layout(
                title=asset_name,
                font_size=10,
                title_font_size=16,
                title_xanchor='center',
                title_x=0.5,
                # yaxis_autorange='reversed'
            )
            return style, fig
        else:
            return {'display': 'none'}, go.Figure()

    @app.callback(
        Output('corr-variables', 'options'),
        Output('corr-variables', 'value'),
        Input('corr-variables', 'value'),
        Input('asset', 'value')
    )
    def limit_corr_var_selections(param_list, asset_name):
        """
        Limits the number of selected parameters for correlation to less than the
        specified max_corr_vars
        :param param_list:
        :param asset_name:
        :return:
        """
        ctx = callback_context
        if ctx.triggered[0]['prop_id'].split('.')[0] == 'asset':
            return list(asset_info[asset_name]['Channels'].keys()), list(asset_info[asset_name]['Channels'].keys())[0:2]
        else:
            if len(param_list) > max_corr_vars - 1:
                return [{'label': x, 'value': x, 'disabled': True}
                        for x in list(asset_info[asset_name]['Channels'].keys())], param_list
            else:
                return [{'label': x, 'value': x, 'disabled': False}
                        for x in list(asset_info[asset_name]['Channels'].keys())], param_list
