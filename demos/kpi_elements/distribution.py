from dash import dcc, html, Input, Output, State, dash_table
from dash.dash_table.Format import Format, Scheme, Trim
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
import pandas as pd
import matplotlib.dates as mdates

DEF_LOOKBACK = '30d'


def dist_block(date_min, date_max, mark_freq=None):

    date_min = pd.to_datetime(date_min).tz_localize('UTC')
    date_max = pd.to_datetime(date_max).tz_localize('UTC')
    if not mark_freq:
        mark_freq = ((date_max - date_min) / 10).round('30D')
    dates = pd.date_range(date_min, date_max, freq=mark_freq) + pd.to_timedelta('1D')

    # Conversion to int because https://github.com/plotly/dash-core-components/issues/159
    marks = {int(mdates.date2num(date)): str(date)[:10] for date in dates}

    return dbc.Row([
        dbc.Col([
            html.H5(children='Distribution Analysis', style={'margin-top': 10}),
            dbc.Row([
                dbc.Col([
                    dash_table.DataTable(
                        # data=df.to_dict('records'),
                        # columns=[{"name": i, "id": i} for i in df.columns],
                        id='dist_stats'
                    )
                ],
                    md=5, style={'margin-left': 20}
                ),
                dbc.Col([
                    dcc.Graph(id='dist-graph', style={'height': '300px'})
                ],
                    md=6
                )
            ]),
            dbc.Row([
                dcc.Checklist(['Compare with historical'], [], id='dist-enable-compare',
                              inputStyle={'margin-right': 5, 'margin-top': 20}),
                html.Div([
                    html.H6(children='Select date', style={'margin-top': 10}),
                    # Conversion to int because https://github.com/plotly/dash-core-components/issues/159
                    dcc.Slider(int(mdates.date2num(date_min)), int(mdates.date2num(date_max)),
                               value=int(mdates.date2num(date_max)), marks=marks, id='dist-hist-selector',
                               updatemode='drag', step=1
                               )
                ],
                    id='dist-hist-block', hidden=True, style={'margin-top': 10, 'margin-bottom': 20}
                )
            ])
        ],
            style={'margin-left': 20}
        )
    ])


def dist_callbacks(app, data, asset_info, lookback=None):
    if not lookback:
        lookback = DEF_LOOKBACK

    @app.callback(
        Output('dist-hist-block', 'hidden'),
        Input('dist-enable-compare', 'value')
    )
    def make_dist_hist_block(dist_enable_compare):
        return not len(dist_enable_compare)

    @app.callback(
        Output('dist-graph', 'figure'),
        Output('dist_stats', 'data'),
        Output('dist_stats', 'columns'),
        Input('dist-hist-selector', 'value'),
        Input('dist-enable-compare', 'value'),
        State('asset', 'value')
    )
    def update_dist_table_and_graph(hist_date_index, dist_enable_compare, asset_name):
        current_end_date = data.index[-1]
        current_start_date = current_end_date - pd.to_timedelta(lookback)
        current_data = data.loc[current_start_date:current_end_date]
        current_stats = current_data.describe()
        current_stats.rename(str(current_end_date)[:10], inplace=True)
        if dist_enable_compare:
            hist_end_date = mdates.num2date(hist_date_index)
            hist_start_date = hist_end_date - pd.to_timedelta(lookback)
            hist_data = data.loc[hist_start_date:hist_end_date]
            hist_stats = hist_data.describe()
            hist_stats.rename(str(hist_end_date)[:10], inplace=True)
            all_stats = pd.concat([current_stats, hist_stats], axis=1)
        else:
            all_stats = current_stats.copy()
        all_stats = all_stats.reset_index()
        all_stats.rename(columns={'index': 'Statistic'}, inplace=True)
        columns = [{'name': i, 'id': i, 'type': 'numeric',
                    'format': Format(precision=2, scheme=Scheme.fixed, trim=Trim.yes)}
                   for i in all_stats.columns]

        parameter_name = asset_name[:-9]  # Strip away the ' Analysis' part from the name
        fig = go.Figure()
        bin_size = (max(current_data) - min(current_data))/5
        fig.add_trace(go.Histogram(x=current_data, name=str(current_end_date)[:10],
                                   xbins_size=bin_size))
        if dist_enable_compare:
            fig.add_trace(go.Histogram(x=hist_data, name=str(hist_end_date)[:10],
                                       xbins_size=bin_size))
            # Overlay both histograms
            fig.update_layout(barmode='overlay')
            # Reduce opacity to see both histograms
            fig.update_traces(opacity=0.75)

        fig.update_layout(
            title='%s (%s)' % (parameter_name, asset_info[asset_name]['Channels'][parameter_name]['Units']),
            title_font_size=16,
            title_xanchor='center',
            title_x=0.5,
            margin=dict(t=30, b=0, l=0, r=0),
            legend=dict(yanchor="bottom", y=0.9, xanchor="left", x=0.01),
        )

        return fig, all_stats.to_dict('records'), columns
