from dash import dcc, html, Input, Output
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
import matplotlib.dates as mdates
import pandas as pd
import numpy as np

PUMP_PERFORMANCE_VARS = ['Head', 'Efficiency', 'Power', 'NPSH']


def pump_perf_block(asset_list, asset_info, date_min, date_max, mark_freq=None):
    default_asset = asset_list[0]
    date_min = pd.to_datetime(date_min).tz_localize('UTC')
    date_max = pd.to_datetime(date_max).tz_localize('UTC')
    if not mark_freq:
        mark_freq = ((date_max - date_min) / 10).round('15D')
    dates = pd.date_range(date_min, date_max, freq=mark_freq) + pd.to_timedelta('1D')

    # Conversion to int because https://github.com/plotly/dash-core-components/issues/159
    marks = {int(mdates.date2num(date)): str(date)[:10] for date in dates}

    return dbc.Row([
        dbc.Col([
            html.H5(children='Pump performance'),
            html.H6(children='Choose parameter'),
            dcc.RadioItems(PUMP_PERFORMANCE_VARS, PUMP_PERFORMANCE_VARS[0],
                           id='pump-perf-map', labelStyle={'display': 'block'},
                           inputStyle={'margin-right': 5}),
            html.H6('Performance Curve for comparison', style={'margin-left': 25, 'margin-top': 10}),
            dcc.Dropdown(list(asset_info[default_asset]['Performance Curves'].keys()),
                         list(asset_info[default_asset]['Performance Curves'].keys())[0],
                         id='pump-curves', style={'margin-left': 10})
        ],
            md=3, style={'margin-left': 20}
        ),
        dbc.Col([
            dbc.Row([
                dbc.Col([dcc.Markdown('Test', id='pump-info-card')], md=3),
                dbc.Col([dcc.Graph(id='pump-perf-map-graph', style={'height': '400px'})], md=9)
            ]),
            html.Div([
                html.H6(children='Select date'),
                # Conversion to int because https://github.com/plotly/dash-core-components/issues/159
                dcc.Slider(int(mdates.date2num(date_min)), int(mdates.date2num(date_max)),
                           value=int(mdates.date2num(date_max)), marks=marks, id='pump-time-selector',
                           updatemode='drag', step=1
                           )
            ],
                style={'margin-left': 55, 'margin-right': 115}
            ),
        ],
            md=8
        ),
    ])


def pump_callbacks(app, data, asset_info, lookback='7d'):
    @app.callback(
        Output('pump-curves', 'options'),
        Output('pump-curves', 'value'),
        Input('asset', 'value')
    )
    def update_available_comp_curves(asset_name):
        curve_list = list(asset_info[asset_name]['Performance Curves'].keys())
        curve_selected = curve_list[0]
        return curve_list, curve_selected

    @app.callback(
        Output('pump-info-card', 'children'),
        Input('asset', 'value'),
        Input('pump-time-selector', 'value'),
    )
    def update_info_card(asset_name, date_index):
        end_date = mdates.num2date(date_index)
        start_date = end_date - pd.to_timedelta(lookback)
        filtered_data = data[asset_name].loc[start_date:end_date]
        output = ''
        output = output + '#### ' + asset_name + '\n'
        output = output + '##### Performance summary\n'
        output = output + '##### %s to %s\n' % (str(start_date)[:10], str(end_date)[:10])
        output = output + '###### Median statistics\n'
        props = ['Vol. Flow Rate', 'Head', 'Head deviation', 'Efficiency',
                 'Efficiency deviation', 'NPSH', 'Power']
        formats = ['%i', '%i', '%i', '%0.1f', '%0.1f', '%0.1f', '%0.1f']
        for prop, outformat in zip(props, formats):
            val = filtered_data[prop].median()
            if not np.isnan(val):
                output = output + '* %s: ' % prop + outformat % (
                    val) + ' %s\n' % asset_info[asset_name]['Channels'][prop]['Units']
            else:
                output = output + '* %s: No data\n' % prop
        return output

    @app.callback(
        Output('pump-perf-map-graph', 'figure'),
        Input('asset', 'value'),
        Input('pump-perf-map', 'value'),
        Input('pump-time-selector', 'value'),
        Input('pump-curves', 'value')
    )
    def update_perf_map(asset_name, perf_variable, date_index, curve_selection):
        end_date = mdates.num2date(date_index)
        start_date = end_date - pd.to_timedelta(lookback)
        x_data = data[asset_name]['Vol. Flow Rate'].loc[start_date:end_date]
        y_data = data[asset_name][perf_variable].loc[start_date:end_date]
        x_data_perf_curve = asset_info[asset_name]['Performance Curves'][curve_selection][perf_variable]['Vol. Flow Rate']
        y_data_perf_curve = asset_info[asset_name]['Performance Curves'][curve_selection][perf_variable][perf_variable]

        fig1 = go.Figure()
        marker_colour = ['rgba(0, 0, 255, %0.3f)' % x for x in np.linspace(0.2, 1, num=len(x_data))]
        fig1.add_trace(go.Scatter(x=x_data, y=y_data, name='Operating Points', mode='markers',
                                  marker=dict(size=14, color=marker_colour)))
        fig1.add_trace(go.Scatter(x=x_data_perf_curve, y=y_data_perf_curve,
                                  name='Perf. Curve: ' + curve_selection, mode='lines+markers'))
        fig1.update_layout(
            title='%s (%s)' % (perf_variable, asset_info[asset_name]['Channels'][perf_variable]['Units']),
            title_font_size=16,
            title_xanchor='center',
            title_x=0.5,
            margin=dict(t=0, b=0, l=0, r=0),
            xaxis_title='Vol. Flow Rate (%s)' % asset_info[asset_name]['Channels']['Vol. Flow Rate'][
                'Units'],
            legend=dict(yanchor="bottom", y=1.0, xanchor="left", x=0.01),
            showlegend=True
        )
        return fig1
