from dash import dcc, html
import dash_bootstrap_components as dbc


def asset_block(asset_list):
    default_asset = asset_list[0]
    return dbc.Row([
        dbc.Col([html.H5(children='Asset', style={'margin-top': 10})], md=2),
        dbc.Col([dcc.Dropdown(asset_list, default_asset, id='asset')], md=5),
    ],
        style={'width': '50%', 'margin-left': 20}
    )
