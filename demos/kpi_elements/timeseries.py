from dash import dcc, html, Input, Output, callback_context
import dash_bootstrap_components as dbc
from plotly.subplots import make_subplots
import plotly.graph_objs as go

FILTER_OPTIONS = ['<', '<=', '>', '>=', '==', '!=']


def ts_block(asset_list, asset_info, max_ts_vars=2):
    default_asset = asset_list[0]
    channel_list = list(asset_info[default_asset]['Channels'].keys())
    return dbc.Row([
        dbc.Col([
            html.H5(children='Time-series Analysis'),
            html.H6(children='Select channels (max. %i)' % max_ts_vars, style={'margin-top': 10}),
            dcc.Dropdown(channel_list, channel_list[0:1], id='ts-variables', multi=True,
                         style={'width': '90%'}),
            dcc.Checklist(['Apply filter'], [], id='ts-enable-filter',
                          inputStyle={'margin-right': 5, 'margin-top': 20}),
            html.Div([
                dbc.Row([
                    dbc.Col([
                        dcc.Dropdown(channel_list, channel_list[0], id='ts-filter-var',
                                     style={'display': 'inline-block', 'width': '200px'}),
                    ]),
                    dbc.Col([
                        dcc.Dropdown(FILTER_OPTIONS, '>', id='ts-filter-condition',
                                     style={'display': 'inline-block', 'width': '50px'}),
                    ]),
                    dbc.Col([
                        dcc.Input(id='ts-filter-value', value=0, type='number', debounce=True, style={'width': '80px'})
                    ],
                        style={'width': '80px'}
                    )
                ])
            ],
                id='ts-filter-block', hidden=True, style={'margin-top': 10, 'margin-bottom': 20}
            ),
        ],
            md=3, style={'margin-left': 20}
        ),
        dbc.Col([
            dcc.Graph(id='ts-graph', style={'width': '100%', 'height': '600px'}),
        ],
            md=8
        )
    ])


def ts_callbacks(app, data, asset_info, max_ts_vars=2):

    @app.callback(
        Output('ts-filter-block', 'hidden'),
        Input('ts-enable-filter', 'value')
    )
    def make_ts_filter_visible(ts_enable_filter):
        return not len(ts_enable_filter)

    @app.callback(
        Output('ts-graph', 'figure'),
        Input('asset', 'value'),
        Input('ts-variables', 'value'),
        Input('ts-enable-filter', 'value'),
        Input('ts-filter-var', 'value'),
        Input('ts-filter-condition', 'value'),
        Input('ts-filter-value', 'value')
    )
    def update_ts_graph(asset_name, channels, enable_filter, filter_var, filter_cond, filter_val):
        """
        Update the time series graph with multiple channels on the y-axes.
        NOTE: There is a hard-coded limit of 4 channels
        :param asset_name:
        :param channels:
        :param enable_filter:
        :param filter_var:
        :param filter_cond:
        :param filter_val:
        :return:
        """
        fig1 = make_subplots(specs=[[{"secondary_y": True}]])
        colours = ['blue', 'green', 'red', 'black']
        filtered_data = data[asset_name]
        if enable_filter:
            filtered_data = filtered_data[eval(
                'filtered_data["' + filter_var + '"]' + filter_cond + str(filter_val)
            )]
        filtered_data = filtered_data.reindex(data[asset_name].index)
        for i, ch in enumerate(channels):
            # sec_y = True if i else False
            # colour = 'blue' if i else 'green'
            # fig1.add_trace(go.Scatter(x=data[asset_name].index, y=data[asset_name][ch],
            #                           mode='lines', line=dict(color=colour), name=ch,
            #                           connectgaps=False),
            #                secondary_y=sec_y)
            # median = data[asset_name][ch].median()
            # stdev = data[asset_name][ch].std()
            # ymin = median - 3 * stdev
            # ymax = median + 3 * stdev
            # fig1.update_yaxes(range=[ymin, ymax], secondary_y=sec_y, showgrid=False, zeroline=False,
            #                   title='%s (%s)' % (ch, asset_info[asset_name]['Channels'][ch]['Units'])
            #                   )
            fig1.add_trace(go.Scatter(x=filtered_data.index, y=filtered_data[ch],
                                      mode='lines', line=dict(color=colours[i]),
                                      name='%s (%s)' % (ch, asset_info[asset_name]['Channels'][ch]['Tag']),
                                      connectgaps=False, yaxis='y%i' % (i+1))
                           )
        fig1.update_layout(
            # title=''.join(
            #     [ch + ' (' + asset_info[asset_name]['Channels'][ch]['Tag'] + ')<br>'
            #      for ch in channels]
            # ),
            title=asset_name,
            title_font_size=16,
            title_xanchor='center', title_x=0.5,
            title_yanchor='top', title_y=0.95,
            margin=dict(b=0, l=0, r=0),
            xaxis_range=[data[asset_name].index[0], data[asset_name].index[-1]],
            xaxis_showgrid=False, xaxis_showline=True,
            legend=dict(yanchor="bottom", y=1.0, xanchor="center", font_size=14, x=0.01), showlegend=True
        )
        fig1.update_layout(xaxis_domain=[0.1, 0.9])
        for i, ch in enumerate(channels):
            axis_label = '%s (%s)' % (ch, asset_info[asset_name]['Channels'][ch]['Units'])
            colour_dict = dict(color=colours[i])
            lw = 1
            sg = False  # showgrid
            zl = False  # zeroline
            sl = True  # showline
            if i == 0:
                fig1.update_layout(yaxis=dict(
                    title=axis_label, titlefont=colour_dict, tickfont=colour_dict,
                    showgrid=sg, zeroline=zl, showline=sl, linewidth=lw, linecolor=colours[i]
                ))
            elif i == 1:
                fig1.update_layout(yaxis2=dict(
                    title=axis_label, titlefont=colour_dict, tickfont=colour_dict,
                    showgrid=sg, zeroline=zl, showline=sl, linewidth=lw, linecolor=colours[i],
                    anchor='x', overlaying='y', side='right'
                ))
            elif i == 2:
                fig1.update_layout(yaxis3=dict(
                    title=axis_label, titlefont=colour_dict, tickfont=colour_dict,
                    showgrid=sg, zeroline=zl, showline=sl, linewidth=lw, linecolor=colours[i],
                    anchor='free', overlaying='y', side='left', position=0.01
                ))
            elif i == 3:
                fig1.update_layout(yaxis4=dict(
                    title=axis_label, titlefont=colour_dict, tickfont=colour_dict,
                    showgrid=sg, zeroline=zl, showline=sl, linewidth=lw, linecolor=colours[i],
                    anchor='free', overlaying='y', side='right', position=0.99
                ))

        return fig1

    @app.callback(
        Output('ts-variables', 'options'),
        Output('ts-variables', 'value'),
        Input('ts-variables', 'value'),
        Input('asset', 'value')
    )
    def update_available_ts_channels(param_list, asset_name):
        """
        Set channel list based on chosen asset
        :param param_list:
        :param asset_name:
        :return:
        """
        ctx = callback_context
        if ctx.triggered[0]['prop_id'].split('.')[0] == 'asset':
            return list(asset_info[asset_name]['Channels'].keys()), list(asset_info[asset_name]['Channels'].keys())[0:1]
        else:
            if len(param_list) > max_ts_vars - 1:
                return [{'label': x, 'value': x, 'disabled': True}
                        for x in list(asset_info[asset_name]['Channels'].keys())], param_list
            else:
                return [{'label': x, 'value': x, 'disabled': False}
                        for x in list(asset_info[asset_name]['Channels'].keys())], param_list
