from app_def import KpiApp

if __name__ == '__main__':
    app = KpiApp('kpi_spec_hr.json')
    app.deploy('0.0.0.0', 8050)
