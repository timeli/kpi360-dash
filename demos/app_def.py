import json
import pandas as pd
from dash import Dash, html
import dash_bootstrap_components as dbc

from kpi_elements import *

external_stylesheets = [dbc.themes.SPACELAB,
                        'https://bootswatch.com/5/spacelab/bootstrap.min.css',
                        "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css",
                        'https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css']

REPEATABLE_ELEMENTS = ['header', 'separator']
ALL_ELEMENTS = ['header', 'separator', 'assetSelector', 'compressorPerformance',
                'pumpPerformance', 'timeSeries', 'correlation', 'distribution',
                'contributors']

CONTRIB_LINKED_APP_DETAILS = {
    'HPT Efficiency': {'port': '8055', 'running': False},
    'IPT St1 Efficiency': {'port': '8060', 'running': False},
    'IPT St2 Efficiency': {'port': '8070', 'running': False},
    'LPT St1 Efficiency': {'port': '8080', 'running': False},
    'LPT St2 Efficiency': {'port': '8090', 'running': False},
    'LPT St3 Efficiency': {'port': '8100', 'running': False},
    'Condenser UA': {'port': '8110', 'running': False},
}

CONTRIB_LINKED_APP_TRIGGER = 'python ./run_kpi_dynamic.py'


def data_placeholder(app_name, date_min, date_max):
    if 'Compressor' in app_name:
        data_dir = './comp_data/'
    elif 'Pump' in app_name:
        data_dir = './pump_data/'
    elif 'Heat Rate' in app_name:
        data_dir = './hr_data/'
    else:
        data_dir = './hr_data/' + app_name + '/'

    with open(data_dir + 'asset_info.json', 'r') as input_obj:
        asset_info = json.load(input_obj)

    asset_data = {}
    for asset in asset_info:
        print(asset)
        asset_data[asset] = pd.read_csv(
            data_dir + '%s.csv' % asset, index_col=0, parse_dates=True).tz_localize('UTC').loc[
            pd.to_datetime(date_min).tz_localize('UTC'):
            pd.to_datetime(date_max).tz_localize('UTC')
        ]
    return asset_data, asset_info


class KpiApp:
    def __init__(self, spec_file):
        with open(spec_file, 'r') as app_spec_obj:
            self.app_spec = json.load(app_spec_obj)

        self.asset_list = self.app_spec['appParameters']['assetList']
        self.app_name = self.app_spec['appParameters']['appName']
        self.date_min = self.app_spec['appParameters']['dateMin']
        self.date_max = self.app_spec['appParameters']['dateMax']

        self.asset_data, self.asset_info = self._pull_data_and_info()
        self.app = self._initialise_app()
        self._create_app_layout_and_callbacks()

    def deploy(self, server, port):
        self.app.run_server(server, port)

    @staticmethod
    def _initialise_app():
        # app = Dash(__name__, suppress_callback_exceptions=True)
        # app = Dash(__name__, title='KPI360', external_stylesheets=[dbc.themes.GRID])
        return Dash(__name__, title='KPI360', external_stylesheets=external_stylesheets)

    def _pull_data_and_info(self):
        # TODO Data and Info will actually be retrieved based on asset_list, date_min and date_max
        return data_placeholder(self.app_name, self.date_min, self.date_max)

    def _create_app_layout_and_callbacks(self):
        # Check that no elements with callbacks are repeated
        culled_list = [y for x in self.app_spec['layout'] for y in x if y not in REPEATABLE_ELEMENTS]
        culled_set = set()
        # https://stackoverflow.com/questions/9835762/how-do-i-find-the-duplicates-in-a-list-and-create-another-list-with-them
        dupes = [x for x in culled_list if x in culled_set or culled_set.add(x)]
        if len(dupes):
            print('Repeated elements: '+', '.join(dupes))
            print('App will NOT WORK')
            return None

        # Create app layout and initiate callbacks
        app_blocks = []
        for row in self.app_spec['layout']:
            row_blocks = []
            for block in row:
                if block == 'header':
                    new_block = header_block(self.app_name)
                elif block == 'separator':
                    new_block = separator_block()
                elif block == 'assetSelector':
                    new_block = asset_block(self.asset_list)
                elif block == 'contributors':
                    new_block = contrib_block()
                    contrib_callbacks(self.app, self.asset_info, CONTRIB_LINKED_APP_DETAILS,
                                      CONTRIB_LINKED_APP_TRIGGER)
                elif block == 'distribution':
                    try:
                        dist_look_back = self.app_spec['blockParameters'][block]['lookBack']
                    except KeyError:
                        dist_look_back = None
                    try:
                        dist_mark_freq = self.app_spec['blockParameters'][block]['markFreq']
                    except KeyError:
                        dist_mark_freq = None
                    kpi_name = self.asset_list[0][:-9]
                    kpi_data = self.asset_data[self.asset_list[0]][kpi_name]
                    new_block = dist_block(self.date_min, self.date_max, mark_freq=dist_mark_freq)
                    dist_callbacks(self.app, kpi_data, self.asset_info, lookback=dist_look_back)
                elif block == 'compressorPerformance':
                    try:
                        comp_look_back = self.app_spec['blockParameters'][block]['lookBack']
                    except KeyError:
                        comp_look_back = None
                    try:
                        comp_mark_freq = self.app_spec['blockParameters'][block]['markFreq']
                    except KeyError:
                        comp_mark_freq = None
                    new_block = comp_perf_block(self.asset_list, self.asset_info,
                                                self.date_min, self.date_max,
                                                mark_freq=comp_mark_freq)
                    comp_callbacks(self.app, self.asset_data, self.asset_info, lookback=comp_look_back)
                elif block == 'pumpPerformance':
                    try:
                        pump_look_back = self.app_spec['blockParameters'][block]['lookBack']
                    except KeyError:
                        pump_look_back = None
                    try:
                        pump_mark_freq = self.app_spec['blockParameters'][block]['markFreq']
                    except KeyError:
                        pump_mark_freq = None
                    new_block = pump_perf_block(self.asset_list, self.asset_info,
                                                self.date_min, self.date_max,
                                                mark_freq=pump_mark_freq)
                    pump_callbacks(self.app, self.asset_data, self.asset_info, lookback=pump_look_back)
                elif block == 'timeSeries':
                    try:
                        ts_max_vars = self.app_spec['blockParameters'][block]['maxVars']
                    except KeyError:
                        ts_max_vars = None
                    new_block = ts_block(self.asset_list, self.asset_info, max_ts_vars=ts_max_vars)
                    ts_callbacks(self.app, self.asset_data, self.asset_info, max_ts_vars=ts_max_vars)
                elif block == 'correlation':
                    try:
                        corr_max_vars = self.app_spec['blockParameters'][block]['maxVars']
                    except KeyError:
                        corr_max_vars = None
                    try:
                        corr_mark_freq = self.app_spec['blockParameters'][block]['markFreq']
                    except KeyError:
                        corr_mark_freq = None
                    new_block = corr_block(self.asset_list, self.asset_info,
                                           self.date_min, self.date_max,
                                           max_corr_vars=corr_max_vars, mark_freq=corr_mark_freq)
                    corr_callbacks(self.app, self.asset_data, self.asset_info, max_corr_vars=corr_max_vars)
                elif block not in ALL_ELEMENTS:
                    print('Unrecognised element "%s" in layout; ignoring.' % block)
                    new_block = None
                row_blocks.append(dbc.Col([new_block], md=row[block]))
            app_blocks.append(dbc.Row(row_blocks))

        self.app.layout = html.Div(children=app_blocks)
