import dash
from dash import html
from dashboard import dashboard_layout
import dash_bootstrap_components as dbc
from registry import _registry
from kpi_engine.kpi_engine import KPIEngine

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
_registry['kpi_engine'] = KPIEngine()

app.layout = html.Div(
    children=[
        dashboard_layout()
    ])

if __name__ == '__main__':
    app.run_server(debug=True)
