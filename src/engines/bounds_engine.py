import pandas as pd
from engines.engine import Engine


class BoundsEngine(Engine):

    OK = 0
    WARNING = 1
    CRITICAL = 2

    def __init__(self, config):
        self.settings_df = BoundsEngine._load_settings(config)

    def run(self, df):
        result_df = pd.DataFrame(columns=['timestamp', 'kpi_id', 'result'])
        for kpi_id in df.columns.values:
            if kpi_id in self.settings_df.index:
                kpi_settings_row = self.settings_df.loc[kpi_id, :]
                for timestamp, row in df.iterrows():
                    result = BoundsEngine._get_result(row[kpi_id], kpi_settings_row)
                    result_df.loc[len(result_df.index)] = [timestamp, kpi_id, result]
        if result_df.shape[0] > 0:
            result_df.to_csv('db/results_tables/bounds_engine_results.csv', mode='a', header=False, index=False)

    @staticmethod
    def _load_settings(config):
        if config.get('is_active', False):
            settings_df = pd.read_csv('db/settings_tables/bounds_engine_settings.csv')
            settings_df.set_index('kpi_id', drop=True, inplace=True)
            return settings_df
        else:
            return None

    @staticmethod
    def _get_result(value, settings_row):
        if value <= settings_row['critical_lower_limit'] or value >= settings_row['critical_upper_limit']:
            return BoundsEngine.CRITICAL
        elif value <= settings_row['warning_lower_limit'] or value >= settings_row['warning_upper_limit']:
            return BoundsEngine.WARNING
        else:
            return BoundsEngine.OK
