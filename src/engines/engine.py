from abc import ABC, abstractmethod


class Engine(ABC):

    @abstractmethod
    def run(self, df):
        pass
