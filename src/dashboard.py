from dash import html
from dash import dcc
import dash_bootstrap_components as dbc
from dash import callback
from dash import Output, Input
import pandas as pd
from registry import _registry


def dashboard_layout():
    layout = html.Div(
        [
            dbc.Button('Run Engines',
                       id='btn-run-engines',
                       className='Primary'),
            dcc.Loading(
                html.Div(
                    children=[
                        generate_grid(get_latest_results())],
                    id='kpi-grid',
                )
            )
        ])
    return layout


def generate_grid(results_df):
    if results_df.shape[0] > 0:
        grid = [dbc.Row(children=[]) for x in range(results_df.shape[0] // 5 + 1)]
        index = 0
        for i, row in results_df.iterrows():
            grid[index // 5].children.append(dbc.Col(generate_kpi_card(row)))
            index += 1
        return html.Div(grid)
    else:
        return 'No data yet'


def generate_kpi_card(row):
    if row['result'] == 2:
        card_color = 'red'
    elif row['result'] == 1:
        card_color = 'yellow'
    else:
        card_color = 'green'

    return dbc.Card(children=[
        dbc.CardHeader(_registry.get('kpi_engine').get_kpi_name_by_id(row['kpi_id'])),
        dbc.CardBody([row['timestamp']])
    ],
        color=card_color,
        style={'margin': '1em'})


def get_latest_results():
    active_engines = _registry.get('kpi_engine').active_engines
    overall_result_df = None
    for engine in active_engines.keys():
        results_df: pd.DataFrame = pd.read_csv('db/results_tables/{}_results.csv'.format(engine))
        if results_df.shape[0] > 0:
            results_df.timestamp = pd.to_datetime(results_df.timestamp, infer_datetime_format=True)
            results_df = results_df.groupby('kpi_id').agg({'timestamp': 'max', 'result': 'last'}).reset_index()
            if overall_result_df is None:
                overall_result_df = results_df.iloc[:, :]
            else:
                if isinstance(overall_result_df, pd.DataFrame):
                    overall_result_df = pd.concat([overall_result_df, results_df]).groupby('timestamp', 'kpi_id')[
                        'result'].max()

    return overall_result_df


@callback(
    Output('kpi-grid', 'children'),
    [Input('btn-run-engines', 'n_clicks')],
    []
)
def run_engine_button_clicked(n_clicks):
    if n_clicks:
        df = pd.read_csv('db/data.csv')
        df = df.iloc[0:10, :]
        df.set_index('timestamp', drop=True, inplace=True)
        kpis_df = pd.read_csv('db/kpis.csv')
        df.rename(mapper={x: kpis_df[kpis_df['name'] == x].iloc[0, :]['id'] for x in df.columns.values}, axis=1,
                  inplace=True)
        _registry.get('kpi_engine').run(df)
        return generate_grid(get_latest_results())
