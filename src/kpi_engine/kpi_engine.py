import yaml
import importlib
import pandas as pd


class KPIEngine:

    def __init__(self):
        self.active_engines = KPIEngine._get_active_engines()
        self.kpi_id_lookup = KPIEngine._create_kpi_id_lookup()

    @staticmethod
    def run(df):
        engines = KPIEngine._get_active_engines()
        for engine_name in engines.keys():
            module = importlib.import_module('engines.{}'.format(engine_name))
            engine_klass = getattr(module, engines[engine_name]['class'])
            engine_instance = engine_klass(engines[engine_name])
            engine_instance.run(df)

    def get_kpi_name_by_id(self, id):
        return self.kpi_id_lookup.get(id, None)

    # private methods

    @staticmethod
    def _get_active_engines():
        with open('kpi_engine/engine_config.yaml', 'r') as f:
            config = yaml.load(f, Loader=yaml.SafeLoader)
        engines = config['engines']
        active_engines = {}
        for engine_name in engines.keys():
            if engines[engine_name]['is_active']:
                active_engines[engine_name] = (engines[engine_name])
        return active_engines

    @staticmethod
    def _create_kpi_id_lookup():
        kpis_df = pd.read_csv('db/kpis.csv')
        lookup = {x['id']: x['name'] for i, x in kpis_df.iterrows()}
        return lookup
